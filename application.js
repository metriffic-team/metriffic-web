const express = require('express');
const app = express();

app.use(express.static('public'));

app.get('/', (req, res) => {
        res.sendFile('./index.html', { root: __dirname });
});

app.get('/downloads/cli-package', function(req, res) {
	const DOWNLOADS_ROOT = process.env.METRIFFIC_WEBDOWNLOADS_ROOT
	const file_path = `${DOWNLOADS_ROOT}/cli-package/metriffic-cli.tar.gz`;
    res.download(file_path); 
});

app.listen(8000, () => console.log('Metriffic app listening on port 8000!'));

